/*
 * @Author: wangzy
 * @Date: 2022-06-20 09:55:45
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-20 15:18:10
 * @Description:
 */
/**
 * 系统录入的信息也太简单了，程序员和产品经理之间的区别一个简单的career字段怎么能说得清？
 * 我要求这个系统具备给不同工种分配职责说明的功能。也就是说，要给每个工种的用户加上一个个性化的字段，
 * 来描述他们的工作内容
 **/
function Coder(name, age) {
  this.name = name
  this.age = age
  this.career = 'coder'
  this.worker = ['写代码', '改bug']
}
function ProductManager(name, age) {
  this.name = name
  this.age = age
  this.career = 'product manager'
  this.work = ['订会议室', '开会']
}
let wangzy = new Coder('王子一', '30')
let cuiyong = new ProductManager('崔勇', 33)
// 简单工厂模式 找出共性，变与不变
function User(name, age, career, work) {
  this.name = name
  this.age = age
  this.career = career
  this.work = work
}
function Factory(name, age, career) {
  let work
  switch (career) {
    case 'coder':
      work = ['写代码', '写系分', '修Bug']
      break
    case 'product manager':
      work = ['订会议室', '写PRD', '催更']
      break
  }
  return new User(name, age, career, work)
}

let wzy = new Factory('wzy', 30, 'coder')
let cy = new Factory('cy', 33, 'product manager')
console.log(wzy)
console.log(cy)
