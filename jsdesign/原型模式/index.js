/*
 * @Author: wangzy
 * @Date: 2022-06-20 16:04:15
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 14:33:25
 * @Description:
 */
//  class
class Father {
  constructor(name, age) {
    this.name = name
    this.age = age
  }
  getName() {
    console.log(this.name)
    return this.name
  }
  getOwnFn() {
    Father.getName()
  }

  static getName() {
    console.log('我是静态方法')
  }
  #name = '我是私有属性'
  #concat() {
    console.log(`${this.#name} + 被私有方法所调用了`)
  }
  concat() {
    this.#concat()
  }
}
let f = new Father('wzy', 30)
// f.getName()
// f.getOwnFn()
// f.concat()

class Son extends Father {
  constructor(...args) {
    super(...args)
    this.color = 'white'
  }
}

let s = new Son('wzy', 20)
console.log(s)
s.getName()
s.getOwnFn()
s.concat()
