/*
 * @Author: wangzy
 * @Date: 2022-06-28 18:24:33
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 19:03:11
 * @Description: 策略模式
 */
/**
 * 上大促要来了，我们本次大促要做差异化询价。啥是差异化询价？
 * 就是说同一个商品，我通过在后台给它设置不同的价格类型，可以让它展示不同的价格。具体的逻辑如下：
 * 当价格类型为“预售价”时，满 100 - 20，不满 100 打 9 折
 * 当价格类型为“大促价”时，满 100 - 30，不满 100 打 8 折
 * 当价格类型为“返场价”时，满 200 - 50，不叠加
 * 当价格类型为“尝鲜价”时，直接打 5 折
 */
/**
 * 预售价 - pre
 * 大促价 - onSale
 * 返场价 - back
 * 尝鲜价 - fresh
 */
// 垃圾代码  if else
// 询价方法，接受价格标签和原价为入参
function askPrice(tag, originPrice) {
  // 处理预热价
  if (tag === 'pre') {
    if (originPrice >= 100) {
      return originPrice - 20
    }
    return originPrice * 0.9
  }

  // 处理大促价
  if (tag === 'onSale') {
    if (originPrice >= 100) {
      return originPrice - 30
    }
    return originPrice * 0.8
  }

  // 处理返场价
  if (tag === 'back') {
    if (originPrice >= 200) {
      return originPrice - 50
    }
    return originPrice
  }

  // 处理尝鲜价
  if (tag === 'fresh') {
    return originPrice * 0.5
  }
}
// 策略模式正确代码
// 询价处理器
// 定义一个询价处理器对象
const priceProcessor = {
  pre(originPrice) {
    if (originPrice >= 100) {
      return originPrice - 20
    }
    return originPrice * 0.9
  },
  onSale(originPrice) {
    if (originPrice >= 100) {
      return originPrice - 30
    }
    return originPrice * 0.8
  },
  back(originPrice) {
    if (originPrice >= 200) {
      return originPrice - 50
    }
    return originPrice
  },
  fresh(originPrice) {
    return originPrice * 0.5
  },
}
// 询价函数
function askPrice(tag, originPrice) {
  return priceProcessor[tag](originPrice)
}
/***
 * 如此一来，askPrice 函数里的 if-else 大军彻底被咱们消灭了。
 * 这时候如果你需要一个新人价，
 * 只需要给 priceProcessor 新增一个映射关系：
 */
priceProcessor.newUser = function (originPrice) {
  if (originPrice >= 100) {
    return originPrice - 50
  }
  return originPrice
}
