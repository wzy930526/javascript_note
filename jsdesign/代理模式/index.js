/*
 * @Author: wangzy
 * @Date: 2022-06-28 15:41:00
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 16:28:55
 * @Description: 代理模式
 */

const girl = {
  name: '丫丫',
  aboutMe: '又勾勾又丢丢',
  age: 24,
  career: 'teacher',
  fakeAvatar: 'xxx.xxx.png', // 虚假照片
  avatar: 'xxx.xxx.png', // 真是照片
  phone: '139啤酒白酒葡萄酒',
  // 礼物数组
  presents: [],
  // 拒收50块以下的礼物
  bottomValue: 50,
  // 记录最近一次收到的礼物
  lastPresent: {
    type: '巧克力',
    value: 60,
  },
}
// 规定礼物的数据结构由type和value组成
const baseInfo = ['age', 'caeer']
// 花钱才能看的信息
const privateInfo = ['avatar', 'phone']

// 男用户 (大冤种) 信息
const user = {
  name: '大冤种',
  isValidated: true, //
  isVip: true,
}

// 开始忽悠
const huyouLovers = new Proxy(girl, {
  get: function (target, key) {
    if (baseInfo.indexOf(key) !== -1 && !user.isValidated) {
      alert('大冤种快去认证!')
      return
    }
    // .... 此处省略其他套路

    // 此处我们认为只有验证通过的用户才可以购买vip
    if (user.isValidated && !user.isVip) {
      alert('大冤种快去变成vip')
      return
    }
    return target[key]
  },
  set: function (target, key, val) {
    // 最近一次送来的礼物 会尝试赋值给lastPresent字段
    if (key === 'lastPresent') {
      if (val.value < girl.bottomValue) {
        alert('你打发要饭的呢?')
        return
      }
      girl.lastPresent = val
      girl.presents = [...girl.presents, val]
    }
  },
})

let b = huyouLovers.age
console.log(b)
huyouLovers.lastPresent = {
  value: 20,
  type: 'rose',
}
