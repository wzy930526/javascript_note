<!--
 * @Author: wangzy
 * @Date: 2022-06-28 15:48:03
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 16:29:34
 * @Description: 
-->

# 代理模式 婚介所
## 前置知识： ES6中的Proxy
在 ES6 中，提供了专门以代理角色出现的代理器 —— Proxy。它的基本用法如下：
```js
let proxy = new Proxy(obj,handle)
```
第一个参数是我们的目标对象，也就是上文中的“未知妹子”。handler 也是一个对象，用来定义代理的行为，相当于上文中的“婚介所”。当我们通过 proxy 去访问目标对象的时候，handler会对我们的行为作一层拦截，我们的每次访问都需要经过 handler 这个第三方。
## “婚介所”的实现
未知妹子的个人信息，刚问了下我们已经注册了 VIP 的同事哥，大致如下：
```js
const girl = {
  name: '丫丫',
  aboutMe:'又勾勾又丢丢',
  age: 24,
  career: 'teacher',
  fakeAvatar: 'xxx.xxx.png',  // 虚假照片
  avatar: 'xxx.xxx.png',// 真是照片
  phone:'139啤酒白酒葡萄酒'
}
```
 > 婚介所收到了小美的信息，开始营业。大家想，这个姓名、自我介绍、假头像，这些信息大差不差，曝光一下没问题。但是人家妹子的年龄、职业、真实头像、手机号码，是不是属于非常私密的信息了？要想 get 这些信息，平台要考验一下你的诚意了 —— 首先，你是不是已经通过了实名审核？如果通过实名审核，那么你可以查看一些相对私密的信息（年龄、职业）。然后，你是不是 VIP ？只有 VIP 可以查看真实照片和联系方式。满足了这两个判定条件，你才可以顺利访问到别人的全部私人信息，不然，就提醒你去完成认证和VIP购买再来。

```js
// 不花钱就能看的信息
const baseInfo = ['age','caeer']
// 花钱才能看的信息
const privateInfo = ['avatar','phone']

// 男用户 (大冤种) 信息
const user = {
  name: '大冤种',
  isValidated: false,
  isVip: false
} 

// 开始忽悠
const huyouLovers = new Proxy(girl, {
  get: function (target, key) {
    if (baseInfo.indexOf(key) !== -1 && !user.isValidated) {
      alert('大冤种快去认证!')
      return
    }
    // .... 此处省略其他套路

    // 此处我们认为只有验证通过的用户才可以购买vip
    if (user.isValidated && !user.isVip) {
      alert('大冤种快去变成vip')
      return
    }
  },
})
```
> 以上主要是 getter 层面的拦截。假设我们还允许会员间互送礼物，每个会员可以告知婚介所自己愿意接受的礼物的价格下限，我们还可以作 setter 层面的拦截。：

```js
const huyouLovers = new Proxy(girl, {
  get: function (target, key) {
    if (baseInfo.indexOf(key) !== -1 && !user.isValidated) {
      alert('大冤种快去认证!')
      return
    }
    // .... 此处省略其他套路

    // 此处我们认为只有验证通过的用户才可以购买vip
    if (user.isValidated && !user.isVip) {
      alert('大冤种快去变成vip')
      return
    }
    return target[key]
  },
  set: function (target, key, val) {
    // 最近一次送来的礼物 会尝试赋值给lastPresent字段
    if (key === 'lastPresent') {
      if (val.value < girl.bottomValue) {
        alert('你打发要饭的呢?')
        return
      }
      girl.lastPresent = val
      girl.presents = [...girl.presents, val]
    }
  },
})

let b = huyouLovers.age
console.log(b)
huyouLovers.lastPresent = {
  value: 20,
  type: 'rose',
}

```