/*
 * @Author: wangzy
 * @Date: 2022-06-20 16:03:42
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 12:10:31
 * @Description:单例模式
 *
 */
class SingleDog {
  show() {
    console.log('我是一个单例对象')
  }
  static getInstance() {
    if (!SingleDog.instance) {
      SingleDog.instance = new SingleDog()
      return SingleDog.instance
    }
    return SingleDog.instance
  }
}

const s1 = SingleDog.getInstance()
const s2 = SingleDog.getInstance()
console.log(s1 === s2)

class SingleCat {
  show() {
    console.log('我是一只小花猫')
  }
}
SingleCat.getInstance = (function () {
  let instance = null
  return function () {
    if (!instance) {
      instance = new SingleCat()
    }
    return instance
  }
})()

const cat1 = SingleCat.getInstance()
const cat2 = SingleCat.getInstance()
console.log(cat1 === cat2)
