/*
 * @Author: wangzy
 * @Date: 2022-06-23 11:05:58
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-25 23:50:43
 * @Description: javaScript 二叉树遍历
 */

let root = {
  value: 1,
  left: {
    value: 2,
    left: {
      value: 3,
    },
    right: {
      value: 4,
    },
  },
  right: {
    value: 5,
    left: {
      value: 6,
    },
    right: {
      value: 7,
    },
  },
}
// 先序遍历
// 所有遍历函数的入参都是树的根结点对象
function preorder(root) {
  // 递归边界，root 为空
  if (!root) {
    return
  }
  // 输出当前遍历的结点值
  console.log('当前遍历的结点值是：', root.value)
  // 递归遍历左子树
  preorder(root.left)
  // 递归遍历右子树
  preorder(root.right)
}
preorder(root)
// 中序遍历
// 所有遍历函数的入参都是树的根结点对象
function inorder(root) {
  // 递归边界，root 为空
  if (!root) {
    return
  }
  // 递归遍历左子树
  inorder(root.left)
  // 输出当前遍历的结点值
  console.log('当前遍历的结点值是：', root.value)
  // 递归遍历右子树
  inorder(root.right)
}
// inorder(root)
// 后序遍历
function postorder(root) {
  // 递归边界，root 为空
  if (!root) {
    return
  }
  // 递归遍历左子树
  postorder(root.left)
  // 递归遍历右子树
  postorder(root.right)
  // 输出当前遍历的结点值
  console.log('当前遍历的结点值是：', root.value)
}
// postorder(root)
