/*
 * @Author: wangzy
 * @Date: 2022-06-30 16:00:02
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-30 16:09:44
 * @Description:  用 JavaScript 对象 来模拟栈
 */
class Stack {
  constructor() {
    this.count = 0
    this.items = {}
  }
  add(element) {
    this[this.count] = element
    this.count++
  }
  size() {
    return this.count
  }
  isEmpty() {
    return this.count === 0
  }
  peek() {
    //获取栈顶的元素
    this.count--
    const result = this.items[this.count]
    delete this.items[this.count]
    return result
  }

  clear() {
    this.items = {}
    this.count = 0
  }
}
