/*
 * @Author: wangzy
 * @Date: 2022-06-30 14:13:37
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-30 15:59:45
 * @Description: 用 JavaScript 数组 来模拟栈
 */

class Stack {
  constructor() {
    this.items = []
  }
  add(item) {
    return this.items.push(item)
  }
  size() {
    return this.items.length
  }
  isEmpty() {
    return this.items.length === 0
  }
  peek() {
    //获取栈顶的元素
    return this.items[this.items.length - 1]
  }
  remove() {
    if (this.isEmpty()) {
      return undefined
    }
    return this.items.pop()
  }
  clear() {
    this.items = []
  }
}

const stack = new Stack()
console.log(stack.isEmpty())
