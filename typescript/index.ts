/*
 * @Author: wangzy
 * @Date: 2022-06-25 22:13:26
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 14:15:16
 * @Description:
 */

/**
 * 1. 定义一个接口 Comparable，该接口里面定义了一个方法  这个方法入参是b 类型是T,出参是个number类型
 * 2. 定义一个 MyObject类 (Ts中也叫 实现接口) MyObject要基于implements接口来实现，所以定义了compareTo方法
 */
interface Comparable<T> {
  compareTo(b: T): number
}

class MyObject implements Comparable<Persons> {
  age!: number
  compareTo(b: Persons): number {
    if (this.age === b.age) {
      return 0
    }
    return this.age > b.age ? 1 : -1
  }
}
interface Persons {
  age: number
  name?: string
}
let ps: Persons = {
  age: 24,
  name: 'wzy',
}
let ps1: Persons = {
  age: 10,
}
let m = new MyObject()
m.age = 11
let s = m.compareTo(ps)
let s1 = m.compareTo(ps1)
console.log(s)
console.log(s1)
