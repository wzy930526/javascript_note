/*
 * @Author: wangzy
 * @Date: 2022-06-30 16:24:22
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-30 17:55:51
 * @Description: 类
 */

class Point {
  x: number
  y: number
  constructor(x: number, y: number) {
    this.x = x
    this.y = y
  }
  add(o: Point) {
    return new Point(o.x + this.x, o.y + this.y)
  }
}

let p = new Point(10, 10)
let p1 = new Point(20, 20)
let sum = p.add(p1)
console.log(sum)

// 继承
class Point3D extends Point {
  z: number
  constructor(x: number, y: number, z: number) {
    super(x, y)
    this.z = z
  }
  add(p: Point3D) {
    let point2D = super.add(p)
    return new Point3D(point2D.x, point2D.y, p.z + this.z)
  }
}
let p3 = new Point3D(10, 20, 30)
let p4 = new Point3D(1, 2, 3)
let pa = p3.add(p4)
console.log(pa)
