/*
 * @Author: wangzy
 * @Date: 2022-06-30 12:21:43
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-30 13:29:31
 * @Description: 基础类型
 */
// 布尔值
let isDone: boolean = true
console.log(isDone)
// 数字
let code: number = 24
console.log(code)
// 字符串
let str: string = 'Bryant'
console.log(str)
// 数组
// 数字数组
let numArray: number[] = [1, 2, 3, 4]
console.log(numArray)
let numArray1: Array<number> = [5, 6, 7, 8]
console.log(numArray1)

interface Obj {
  name: string
  age?: number
}
let objArray: Obj[] = [
  {
    name: 'wzy',
    age: 24,
  },
  {
    name: 'Bryant',
  },
]
console.log(objArray)
let objArray1: Array<Obj> = [
  {
    name: 'wzy1',
    age: 24,
  },
  {
    name: 'Bryant1',
  },
]
console.log(objArray1)
// 元组
let tuple: readonly [string, number, object] = [
  'wangzy',
  24,
  { name: 'Bryant' },
]
console.log(tuple)

// 枚举
enum Color {
  Red,
  Green,
  Blue,
}
let c: Color = Color.Green
console.log('c', c)
let colorName: string = Color[1]
console.log('colorName', colorName)

// 类型断言
let someValue: string = 'this is a string'
let strLength: number = (<string>someValue).length
console.log(strLength)
// as
interface newObj {
  name: string
  age?: number
}
let someValues: newObj[] = [{ name: 'wangzy' }]
let strLengths: number = (someValues as newObj[]).length
console.log(strLengths)
