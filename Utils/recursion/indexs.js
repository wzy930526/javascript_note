/*
 * @Author: wangzy
 * @Date: 2022-06-21 17:48:21
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-21 20:55:03
 * @Description: 递归
 */

let data1 = {
  name: {
    age: {
      a: () => {
        console.log(222)
      },
    },
    obj: null,
  },
}
let data2 = {
  name: {
    obj: {
      age: {
        a: function () {
          console.log(111)
        },
      },
    },
  },
}

function recursion(data, str) {
  // 判断data 是不是对象
  if (data && Object.keys(data).length) {
    if (data.hasOwnProperty(str)) {
      data[str]()
      return
    }
    for (let key in data) {
      recursion(data[key], str)
    }
  }
}
recursion(data1, 'a')
