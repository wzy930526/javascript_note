/*
 * @Author: wangzy
 * @Date: 2022-06-20 09:56:00
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-25 23:30:38
 * @Description: 防抖
 */
let debunceBtn = document.getElementById('debunceBtn')
debunceBtn.addEventListener('click', debunce(getValue, 3000))
function getValue(e) {
  console.log('getValue: ')
  console.log(this)
  console.log(e)
}
function debunce(fn, ms) {
  let t = null
  return function (e) {
    let that = this
    let isFirst = !t

    if (isFirst) {
      fn.apply(that, arguments)
      // fn()
      t = setTimeout(function () {
        t = null
      }, ms)
    }
  }
}
