/*
 * @Author: wangzy
 * @Date: 2022-06-25 23:23:49
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-25 23:34:24
 * @Description: 节流
 */
let throttleBtn = document.getElementById('throttleBtn')
throttleBtn.addEventListener('click', throttle(getValue, 2000))
function getValue(e) {
  console.log('getValue: ')
  console.log(this)
  console.log(e)
}

function throttle(fn, delay) {
  let last, deferTimer
  return function (e) {
    let that = this
    let now = +new Date()
    if (last && now < last + delay) {
      clearTimeout(deferTimer)
      deferTimer = setTimeout(function () {
        last = now
        fn.apply(that, arguments)
      }, delay)
    } else {
      last = now
      fn.apply(that, arguments)
    }
  }
}
