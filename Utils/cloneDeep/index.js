/*
 * @Author: wangzy
 * @Date: 2022-06-22 15:00:51
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-22 15:14:19
 * @Description: 深拷贝
 */
// 定义检测数据类型的功能函数
const checkedType = (target) => {
  return Object.prototype.toString.call(target).slice(8, -1)
}
// 实现深度克隆对象或者数组
const deepClone = (target) => {
  // 判断拷贝的数据类型
  // 初始化变量 result 成为最终数据
  let result,
    targetType = checkedType(target)
  if (targetType === 'Object') {
    result = {}
  } else if (targetType === 'Array') {
    result = []
  } else {
    return target
  }

  // 遍历目标数据
  for (let i in target) {
    // 获取遍历数据结构的每一项值
    let value = target[i]
    // 判断目标结构里的每一项值是否存在对象或者数组
    if (checkedType(value) === 'Object' || checkedType(value) === 'Array') {
      // 如果对象或者数组中还嵌套了对象或者数组，那么继续遍历
      result[i] = deepClone(value)
    } else {
      // 否则直接赋值
      result[i] = value
    }
  }

  // 返回最终值
  return result
}
// let obj = {
//   name: 'wzy',
// }
// let ary = [1, 2, 3, 4]
// let str = 'wzy'
// let num = 12
// let fn = function () {}
// let reg = /\./
// console.log(Object.prototype.toString.call(obj).slice(8, -1))
// console.log(Object.prototype.toString.call(ary).slice(8, -1))
// console.log(Object.prototype.toString.call(str).slice(8, -1))
// console.log(Object.prototype.toString.call(num).slice(8, -1))
// console.log(Object.prototype.toString.call(fn).slice(8, -1))
// console.log(Object.prototype.toString.call(reg).slice(8, -1))
