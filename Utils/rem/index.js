/*
 * @Author: wangzy
 * @Date: 2022-06-20 09:57:14
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-20 14:30:21
 * @Description:
 */

function remSize() {
  let deviceWidth = document.documentElement.clientWidth || window.clientWidth
  if (deviceWidth >= 750) {
    deviceWidth = 750
  }
  if (750 > deviceWidth >= 375) {
    deviceWidth = 375
  }
  if (deviceWidth < 320) {
    deviceWidth = 320
  }
  document.documentElement.style.fontSize = deviceWidth / 7.5 + 'px'
  document.querySelector('body').style.fontSize = 0.3 + 'rem'
}
remSize()
window.addEventListener('resize', function () {
  remSize()
})
