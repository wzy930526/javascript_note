// 1.1 p1的状态作为p2
const p1 = new Promise(function (resolve, reject) {
    setTimeout(() => resolve('hello world'), 3000)
})

const p2 = new Promise(function (resolve, reject) {
    setTimeout(() => {
        resolve(p1)
    }, 1000)
    setTimeout(() => {
        reject(new Error('sad world'))
    }, 2000)
})
p2.then((result) => console.log('result', result)).catch((error) => console.log('error', error))

// 1.2 resolve后面的函数还会执行
new Promise((resolve, reject) => {
    resolve(1)
    console.log(2, new Date().getTime())
}).then((r) => {
    console.log(r, new Date().getTime())
})
// 1.3 resolve加上return，后面的不会再执行
new Promise((resolve, reject) => {
    return resolve(1)
    console.log(2, new Date().getTime())
}).then((r) => {
    console.log(r, new Date().getTime())
})
