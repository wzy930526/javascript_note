/*
 * @Author: wangzy
 * @Date: 2022-06-20 16:05:49
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 13:43:02
 * @Description: Class
 */
let methodName = 'getArea'
class Person {
  constructor(name, age) {
    this.name = name
    this.age = age
  }
  myName() {
    return `my name is ${this.name}`
  }
  get remark() {
    return `my name is ${this.name},${this.age} years old`
  }
  set remark(value) {
    this.age = value
  }
  [methodName]() {
    console.log('beijing')
  }
}

Object.assign(Person.prototype, {
  wzy() {
    return this.name
  },
  setwzy(value) {
    this.name = value
  },
})

const p = new Person('wzy', 30)
p.remark = 10
p.setwzy('Beckham')
// console.log(Object.keys(Person.prototype))
// console.log(Object.getOwnPropertyNames(Person.prototype))
p.getArea()
