/*
 * @Author: wangzy
 * @Date: 2022-06-20 16:06:13
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 11:47:22
 * @Description: Reflect
 */

// Reflect.get方法查找并返回target对象的name属性，如果没有该属性，则返回undefined。
let myObject = {
  name: 'wzy',
  age: 30,
  get remark() {
    return `I am ${this.name},${this.age} years old`
  },
}
let name = Reflect.get(myObject, 'name')
let age = Reflect.get(myObject, 'age')
let remark = Reflect.get(myObject, 'remark')
let job = Reflect.get(myObject, 'job')
// console.log(job)

// 如果name属性部署了读取函数（getter），则读取函数的this绑定receiver
let receiver_obj = {
  name: 'kobe',
  age: 44,
}
let receiver_remark = Reflect.get(myObject, 'remark', receiver_obj)
// console.log(receiver_remark)

// Reflect.set方法设置target对象的name属性等于value

let myObject1 = {
  name: 'wzy',
  age: 30,
  set setName(value) {
    return (this.name = value)
  },
}
console.log(myObject1.name)
Reflect.set(myObject1, 'name', 'wangzy')
console.log(myObject1.name)
Reflect.set(myObject1, 'setName', 'Bryant')
console.log(myObject1.name)
// 如果name属性设置了赋值函数，则赋值函数的this绑定receiver。
let receiver_obj1 = {
  name: 'Charles',
  age: 10,
}
console.log(receiver_obj1.name)
Reflect.set(myObject1, 'setName', 'Beckham', receiver_obj1)
console.log(receiver_obj1.name)
