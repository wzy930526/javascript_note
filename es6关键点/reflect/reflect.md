<!--
 * @Author: wangzy
 * @Date: 2022-06-28 11:24:17
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 11:48:07
 * @Description: Reflect
-->
# Reflect

## 静态方法
1. Reflect 共有13个静态方法
```js
Reflect.apply(target, thisArg, args)
Reflect.construct(target, args)
Reflect.get(target, name, receiver)
Reflect.set(target, name, value, receiver)
Reflect.defineProperty(target, name, desc)
Reflect.deleteProperty(target, name)
Reflect.has(target, name)
Reflect.ownKeys(target)
Reflect.isExtensible(target)
Reflect.preventExtensions(target)
Reflect.getOwnPropertyDescriptor(target, name)
Reflect.getPrototypeOf(target)
Reflect.setPrototypeOf(target, prototype)
```
### Reflect.get(target, name, receiver) 
- Reflect.get方法查找并返回target对象的name属性，如果没有该属性，则返回undefined。
  ```js
  let myObject = {
  name: 'wzy',
  age: 30,
  get remark() {
    return `I am ${this.name},${this.age} years old`
    },
  } 
  let name = Reflect.get(myObject, 'name')
  let age = Reflect.get(myObject, 'age')
  let remark = Reflect.get(myObject, 'remark')
  ```
- 如果name属性部署了读取函数（getter），则读取函数的this绑定receiver
  
  ```js
  let receiver_obj = {
    name: 'kobe',
    age: 44,
  }
  let receiver_remark = Reflect.get(myObject, 'remark', receiver_obj)
  console.log(receiver_remark)
  ```
### Reflect.set(target, name, value, receiver) 
- Reflect.set方法设置target对象的name属性等于value
  ```js
  let myObject1 = {
  name: 'wzy',
  age: 30,
  set setName(value) {
    return (this.name = value)
    },
  }
  console.log(myObject1.name)
  Reflect.set(myObject1, 'name', 'wangzy')
  console.log(myObject1.name)
  Reflect.set(myObject1, 'setName', 'Bryant')
  console.log(myObject1.name)
  ```
- 如果name属性设置了赋值函数，则赋值函数的this绑定receiver。
  ```js
  let receiver_obj1 = {
  name: 'Charles',
  age: 10,
  }
  console.log(receiver_obj1.name)
  Reflect.set(myObject1, 'setName', 'Beckham', receiver_obj1)
  console.log(receiver_obj1.name)
  ```