let list = [1, 1, 1, 2, 3, 2, 1, 3, 4]
let set = [...new Set(list)]
let set1 = Array.from(new Set(list))
// console.log('set', set) // 可用于数组去重
// console.log('set1', set1) // 可用于数组去重

// Set 实例的属性和方法
let set2 = new Set([1, 2, 3, 4, 5, 52, 2, 3, 4, 2, 2])
// 向set结构中后面添加数据
let addSet = set2.add(33)
console.log(addSet)
// 获取set 结构的大小
let setSize = set2.size
console.log(setSize)
//删除set结构某个值
let del
