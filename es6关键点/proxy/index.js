/*
 * @Author: wangzy
 * @Date: 2022-06-20 16:06:07
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-28 15:11:47
 * @Description: Proxy
 *
 */
let obHandle = {
  get: function (target, propKey) {
    console.log('get')
    return target[propKey]
  },
  set: function (target, propKey, value, receiver) {
    console.log('set')
    target[propKey] = value
    Reflect.set(target, propKey, value, receiver)
  },
  defineProperty(target, propKey, attribute) {
    console.log('defineProperty')
    Reflect.defineProperty(target, propKey, attribute)
  },
}

let obj = new Proxy({}, obHandle)
obj.name = '_wzy'
obj.age = 28
++obj.age
console.log(obj)

let proxy = new Proxy(
  {},
  {
    get: function (target, key) {
      return 'hahaha'
    },
  }
)
console.log(proxy.name)
